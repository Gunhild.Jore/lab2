package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    private List<FridgeItem> content = new ArrayList<>();

    private final int maxSize = 20;

    @Override
    public int nItemsInFridge() {
        
        return content.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() != totalSize()) {
            content.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!content.remove(item)){
            throw new NoSuchElementException("Dust du har ikke det!");
        }
    }

    @Override
    public void emptyFridge() {
        content.removeAll(content);
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<>();
        for(FridgeItem item: content){
            if (item.hasExpired()){
                expiredFood.add(item);
            }
        }
        content.removeAll(expiredFood);
        return expiredFood;
    }
    
}



